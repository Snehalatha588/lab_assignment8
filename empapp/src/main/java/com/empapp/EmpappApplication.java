package com.empapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.empapp.entities.Employee;
import com.empapp.service.EmployeeService;

@SpringBootApplication
public class EmpappApplication implements CommandLineRunner {

	@Autowired
	private EmployeeService empService;

	public static void main(String[] args) {
		SpringApplication.run(EmpappApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
//		System.out.println("rec are saved...");
//		empService.save(new Employee("venkat", 45));
//		empService.save(new Employee("laxmi", 36));
//		empService.save(new Employee("sneha", 20));
//		empService.save(new Employee("jyotshna", 17));

	}

}
